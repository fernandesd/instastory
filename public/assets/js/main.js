$(".generate_feed").on('click', function () {

  const procurar = $('#procurar').val();
  const quantidade = $('#quantidade').val();

  $(".feed").instastory({
    get: procurar,
    limit: quantidade,
    template: '<div class="col-md-2 mb-5"><a href="{{link}}" target="_blank" rel="noopener noreferrer"><img src="{{image}}"alt="{{caption}}"></a></div>'
  });

});

